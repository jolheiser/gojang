package gojang

import (
	"fmt"
	"testing"
	"time"
)

func TestUsernameToUUID(t *testing.T) {

	tt := []struct {
		Username string
		UUID     string
		At       time.Time
	}{
		{
			Username: "Etzelia",
			UUID:     "bf0446a896954c41aa4c7ff45bfd1171",
			At:       time.Now(),
		},
		{
			Username: "Mygelia",
			UUID:     "de10775bfcb14665bce45924b2e30188",
			At:       time.Date(2015, 5, 1, 0, 0, 0, 0, time.UTC),
		},
	}

	for _, tc := range tt {
		t.Run(fmt.Sprintf("%s at %s", tc.Username, tc.At), func(t *testing.T) {
			resp, err := gojang.Profile(tc.Username, tc.At)
			if err != nil {
				t.Log(err)
				t.FailNow()
			}

			if resp.UUID != tc.UUID {
				t.Logf("incorrect UUID for %s", tc.Username)
				t.FailNow()
			}
		})
	}

}
