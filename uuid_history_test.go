package gojang

import (
	"testing"
	"time"
)

func TestUUIDToNameHistory(t *testing.T) {
	tt := []struct {
		UUID           string
		NumNames       int
		CurrentChanged time.Time
	}{
		{
			UUID:           "bf0446a896954c41aa4c7ff45bfd1171",
			NumNames:       1,
			CurrentChanged: time.Unix(0, 0),
		},
		{
			UUID:           "de10775bfcb14665bce45924b2e30188",
			NumNames:       3,
			CurrentChanged: time.Unix(1446169803, 0),
		},
	}

	for _, tc := range tt {
		t.Run(tc.UUID, func(t *testing.T) {
			history, err := gojang.UUIDToNameHistory(tc.UUID)
			if err != nil {
				t.Log(err)
				t.FailNow()
			}

			if len(history) != tc.NumNames {
				t.Logf("expected %d names, but got %d", tc.NumNames, len(history))
				t.FailNow()
			}

			if !history.Current().ChangedToAtTime().Equal(tc.CurrentChanged) {
				t.Log("current name changedToAt did not match")
				t.FailNow()
			}
		})
	}
}
