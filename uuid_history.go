package gojang

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"
)

type NameHistoryEntry struct {
	Name        string `json:"name"`
	ChangedToAt int64  `json:"changedToAt"`
}

func (n NameHistoryEntry) ChangedToAtTime() time.Time {
	return time.Unix(n.ChangedToAt/1000, 0)
}

type NameHistoryResponse []NameHistoryEntry

func (n NameHistoryResponse) Current() NameHistoryEntry {
	return n[len(n)-1]
}

func (n NameHistoryResponse) First() NameHistoryEntry {
	return n[0]
}

// UUID -> Name History
func (g *Gojang) UUIDToNameHistory(uuid string) (NameHistoryResponse, error) {
	var nameHistoryResponse NameHistoryResponse
	endpoint := fmt.Sprintf("%s/user/profiles/%s/names", API, uuid)

	resp, err := g.get(endpoint)
	if err != nil {
		return nameHistoryResponse, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nameHistoryResponse, err
	}
	defer resp.Body.Close()

	if err := json.Unmarshal(body, &nameHistoryResponse); err != nil {
		return nameHistoryResponse, fmt.Errorf("could not read JSON response: %v", err)
	}

	return nameHistoryResponse, nil
}
