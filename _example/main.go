package main

import (
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"gitea.com/jolheiser/gojang"
	"gitea.com/jolheiser/gojang/query"
)

func main() {
	username := flag.String("username", "Etzelia", "A username to get history for")
	at := flag.String("at", "", "At a specific time (format 01-30-2006)")
	flag.Parse()

	doAPI(username, at)
	doPing()
	doQuery()
}

func doAPI(username, at *string) {
	atTime := time.Now()
	if *at != "" {
		t, err := time.Parse("01-02-2006", *at)
		if err != nil {
			log.Fatal(err)
		}
		atTime = t
	}

	client := gojang.New(time.Second * 5)

	// Convert username to profile
	profile, err := client.Profile(*username, atTime)
	if err != nil {
		log.Fatal(err)
	}

	// Convert UUID to name history
	names, err := client.UUIDToNameHistory(profile.UUID)
	if err != nil {
		log.Fatal(err)
	}

	// Some convenience funcs
	fmt.Printf("Original name: %s\n", names.First().Name)
	fmt.Printf(" Current name: %s\n\n", names.Current().Name)

	// Names should be in order from first -> last
	for _, name := range names {
		changedToAt := "Original"
		if name.ChangedToAt != 0 {
			t := name.ChangedToAtTime()
			changedToAt = fmt.Sprintf("Changed on %s at %s", t.Format("Mon, Jan 2 2006"), t.Format("03:04:05 PM"))
		}
		fmt.Printf("%s\n\t%s\n\n", name.Name, changedToAt)
	}
}

func doPing() {
	server := query.NewServer("play.birbmc.com", 25565)
	ping, err := server.Ping(query.DefaultTimeout, query.DefaultDeadline)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Version: %s\n", ping.Version)
	fmt.Printf("MOTD: %s\n", ping.MOTD)
	fmt.Printf("Current Players: %d\n", ping.CurrentPlayers)
	fmt.Printf("Max Players: %d\n", ping.MaxPlayers)
	fmt.Printf("Latency: %s\n", ping.Latency)
}

func doQuery() {
	server := query.NewServer("play.birbmc.com", 25565)
	q, err := server.Query(query.DefaultTimeout, query.DefaultDeadline)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Mod: %s\n", q.ServerMod)
	fmt.Printf("Map: %s\n", q.Map)
	plugins := make([]string, len(q.Plugins))
	for idx, pl := range q.Plugins {
		plugins[idx] = pl.String()
	}
	fmt.Printf("Plugins: %s\n", strings.Join(plugins, ", "))
	fmt.Printf("Players: %s\n", strings.Join(q.Players, ", "))
}
