package gojang

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Status string

const (
	StatusGreen   Status = "green"
	StatusYellow  Status = "yellow"
	StatusRed     Status = "red"
	StatusUnknown Status = "unknown"
)

func ParseStatus(status string) Status {
	switch status {
	case "green":
		return StatusGreen
	case "yellow":
		return StatusYellow
	case "red":
		return StatusRed
	default:
		return StatusUnknown
	}
}

type StatusCheck struct {
	MinecraftNet           Status
	SessionMinecraftNet    Status
	AccountMojangCom       Status
	AuthServerMojangCom    Status
	SessionServerMojangCom Status
	APIMojangCom           Status
	TexturesMinecraftNet   Status
	MojangCom              Status
}

func (g *Gojang) Status() (StatusCheck, error) {
	var check StatusCheck
	resp, err := g.get("https://status.mojang.com/check")
	if err != nil {
		return check, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return check, err
	}
	defer resp.Body.Close()

	m := make([]map[string]string, 0)
	if err := json.Unmarshal(body, &m); err != nil {
		return check, fmt.Errorf("could not read JSON response: %v", err)
	}

	for _, s := range m {
		for k, v := range s {
			status := ParseStatus(v)
			switch k {
			case "minecraft.net":
				check.MinecraftNet = status
			case "session.minecraft.net":
				check.SessionMinecraftNet = status
			case "account.mojang.com":
				check.AccountMojangCom = status
			case "authserver.mojang.com":
				check.AuthServerMojangCom = status
			case "sessionserver.mojang.com":
				check.SessionServerMojangCom = status
			case "api.mojang.com":
				check.APIMojangCom = status
			case "textures.minecraft.net":
				check.TexturesMinecraftNet = status
			case "mojang.com":
				check.MojangCom = status
			}
		}
	}

	return check, nil
}
