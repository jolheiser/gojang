package gojang

import (
	"io"
	"net/http"
	"time"

	"gitea.com/jolheiser/gojang/rate"
)

const API = "https://api.mojang.com"

type Gojang struct {
	client http.Client
	rate   *rate.Limit
}

// Get needs a wrapper because Mojang doesn't like the default Go user agent
func (g *Gojang) get(url string) (*http.Response, error) {
	if err := g.rate.Try(); err != nil {
		return nil, err
	}
	req, err := newRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	return g.client.Do(req)
}

// Post needs a wrapper because Mojang doesn't like the default Go user agent
func (g *Gojang) post(url string, body io.Reader, headers map[string]string) (*http.Response, error) {
	if err := g.rate.Try(); err != nil {
		return nil, err
	}
	req, err := newRequest(http.MethodPost, url, body)
	if err != nil {
		return nil, err
	}
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	return g.client.Do(req)
}

func newRequest(method string, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "Gojang")
	return req, nil
}

func New(timeout time.Duration) *Gojang {
	return &Gojang{
		client: http.Client{
			Timeout: timeout,
		},
		rate: rate.NewLimit(600, time.Minute*10),
	}
}
