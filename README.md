# Gojang

Mojang API library

[Reference](https://wiki.vg/Mojang_API)


## API Implementation

- [x] Status
- [x] Username -> UUID
- [x] UUID -> Name History
- [x] Playernames -> UUIDs
- [ ] UUID -> Profile + Skin/Cape
- [ ] Change Skin
- [ ] Upload Skin
- [ ] Reset Skin
- [ ] Security question-answer flow
- [ ] Blocked Servers
- [ ] Statistics

## Protocol Implementation

- [x] Ping
- [x] Query

## Example

[Example File](_example/main.go)

Name History  
`go run _example/main.go --username <your username>`

If you only know what their username was at a specific time  
`go run _example/main.go --username <your username> --at <01-02-2006>`