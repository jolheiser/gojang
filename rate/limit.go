package rate

import (
	"fmt"
	"sync"
	"time"
)

type Limit struct {
	limit     int
	timeFrame time.Duration
	times     []time.Time
	mx        sync.Mutex
}

func NewLimit(limit int, timeFrame time.Duration) *Limit {
	return &Limit{
		limit:     limit,
		timeFrame: timeFrame,
		times:     make([]time.Time, 0),
	}
}

func (l *Limit) Try() error {
	l.mx.Lock()
	defer l.mx.Unlock()
	now := time.Now()

	// Clean up
	var sub int
	expired := now.Add(-l.timeFrame)
	for idx, t := range l.times {
		if t.Before(expired) {
			sub = idx
		}
	}
	l.times = l.times[sub:]

	if len(l.times) >= l.limit {
		return LimitExceededError{
			// Time from now until the oldest time + the time-frame
			Next: now.Sub(l.times[0].Add(l.timeFrame)),
		}
	}

	l.times = append(l.times, now)
	return nil
}

type LimitExceededError struct {
	Next time.Duration
}

func (r LimitExceededError) Error() string {
	return fmt.Sprintf("rate limit exceeded, try again in %s", r.Next)
}

func IsRateLimitExceededError(err error) bool {
	_, ok := err.(LimitExceededError)
	return ok
}
