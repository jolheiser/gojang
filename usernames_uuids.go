package gojang

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

// Usernames -> UUIDs (+ profiles)
func (g *Gojang) UsernamesToUUIDs(usernames ...string) ([]UsernameResponse, error) {
	usernameResponses := make([]UsernameResponse, 0, len(usernames))
	endpoint := fmt.Sprintf("%s/profiles/minecraft", API)

	// Prune empty usernames to avoid errors
	pruned := make([]string, 0, len(usernames))
	for _, u := range usernames {
		u = strings.TrimSpace(u)
		if u != "" {
			pruned = append(pruned, u)
		}
	}

	p := 1
	for {
		var batch []string
		var stop bool
		if len(usernames) > p*10 {
			batch = usernames[p*10-10 : p*10]
		} else {
			batch = usernames[p*10-10:]
			stop = true
		}

		payload, err := json.Marshal(batch)
		if err != nil {
			return usernameResponses, err
		}

		resp, err := g.post(endpoint, bytes.NewReader(payload), map[string]string{
			"Content-Type": "application/json",
		})
		if err != nil {
			return usernameResponses, err
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return usernameResponses, err
		}
		defer resp.Body.Close()

		var batchResponses []UsernameResponse
		if err := json.Unmarshal(body, &batchResponses); err != nil {
			return usernameResponses, fmt.Errorf("could not read JSON response: %v", err)
		}

		usernameResponses = append(usernameResponses, batchResponses...)
		if stop {
			break
		}
	}

	return usernameResponses, nil
}
