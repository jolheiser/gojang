package gojang

import (
	"os"
	"testing"
	"time"
)

var gojang *Gojang

func TestMain(m *testing.M) {
	gojang = New(time.Second * 5)
	os.Exit(m.Run())
}
