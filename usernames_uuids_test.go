package gojang

import (
	"testing"
)

func TestUsernamesToUUIDs(t *testing.T) {

	tt := []struct {
		Username string
		UUID     string
	}{
		{
			Username: "Etzelia",
			UUID:     "bf0446a896954c41aa4c7ff45bfd1171",
		},
		{
			Username: "NonExistingPlayer",
		},
		{
			Username: "MrsEtzelia",
			UUID:     "de10775bfcb14665bce45924b2e30188",
		},
		{
			Username: "Notch",
			UUID:     "069a79f444e94726a5befca90e38aaf5",
		},
		{
			Username: "Mojang",
			UUID:     "0bec571143164a8fb61b2a119e652082",
		},
		{
			Username: "Jeb_",
			UUID:     "853c80ef3c3749fdaa49938b674adae6",
		},
		{
			Username: "Grian",
			UUID:     "5f8eb73b25be4c5aa50fd27d65e30ca0",
		},
		{
			Username: "MumboJumbo",
			UUID:     "c7da90d56a054217b94a7d427cbbcad8",
		},
		{
			Username: "xisumavoid",
			UUID:     "8d86df19fa5c4939ac7c3b90b2b6abb6",
		},
		{
			Username: "TangoTek",
			UUID:     "c2faba363f6e4961b8345bcfe5657f72",
		},
		{
			Username: "ilmango",
			UUID:     "52ea935499ed4b06bec2331e7c0f6f57",
		},
		// 11 usernames to test batching
	}

	usernames := make([]string, 0, len(tt))
	for _, t := range tt {
		usernames = append(usernames, t.Username)
	}

	resp, err := gojang.UsernamesToUUIDs(usernames...)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(resp) != 10 {
		t.Logf("returned results were inconsistent with test data")
		t.FailNow()
	}
}
