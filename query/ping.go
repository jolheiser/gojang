package query

import (
	"errors"
	"fmt"
	"net"
	"regexp"
	"strings"
	"time"
)

// Ping is the simple response for a Server
type Ping struct {
	Version        string
	MOTD           string
	CurrentPlayers int
	MaxPlayers     int
	Latency        time.Duration
}

var formatRe = regexp.MustCompile(`[§&�][0-9a-gk-or]`)

// CleanMOTD strips all formatting characters from the MOTD
func (p *Ping) CleanMOTD() string {
	return formatRe.ReplaceAllString(p.MOTD, "")
}

// Ping returns a Ping from a server
func (s *Server) Ping(timeout, deadline time.Duration) (*Ping, error) {
	start := time.Now()
	conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", s.IP, s.Port), timeout)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	if err := conn.SetDeadline(time.Now().Add(deadline)); err != nil {
		return nil, err
	}
	latency := time.Since(start).Round(time.Millisecond)

	if _, err := conn.Write([]byte("\xFE\x01")); err != nil {
		return nil, err
	}

	raw := make([]byte, 512)
	if _, err := conn.Read(raw); err != nil {
		return nil, err
	}

	if len(raw) == 0 {
		return nil, errors.New("no data returned")
	}

	data := strings.Split(string(raw[:]), "\x00\x00\x00")
	if data == nil || len(data) < numFields {
		return nil, fmt.Errorf("could not parse returned data: %s", data)
	}

	return &Ping{
		Version:        fixString(data[2]),
		MOTD:           fixString(data[3]),
		CurrentPlayers: mustInt(data[4]),
		MaxPlayers:     mustInt(data[5]),
		Latency:        latency,
	}, nil
}
