package query

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"strings"
	"time"
)

// Query is the advanced response from the server
type Query struct {
	Ping
	GameType  string
	GameID    string
	ServerMod ServerMod
	Plugins   []Plugin
	Map       string
	HostPort  int
	HostIP    string
	Players   []string
}

// ServerMod is the type of server e.g. Bukkit, Spigot, Paper, etc.
type ServerMod struct {
	Name    string
	Base    string
	Version string
}

// String returns the ServerMod in string format
func (s ServerMod) String() string {
	return fmt.Sprintf("%s on %s %s", s.Name, s.Base, s.Version)
}

// Plugin is a plugin's name and version
type Plugin struct {
	Name    string
	Version string
}

// String returns the Plugin in string format
func (p Plugin) String() string {
	return fmt.Sprintf("%s %s", p.Name, p.Version)
}

func (s *Server) handshake(conn net.Conn) error {
	if _, err := conn.Write([]byte("\xFE\xFD\x09\x00\x00\x00\x01")); err != nil {
		return err
	}

	raw := make([]byte, 512)
	if _, err := conn.Read(raw); err != nil {
		return err
	}

	if len(raw) == 0 {
		return errors.New("no data returned")
	}

	s.challenge = uint32(mustInt(fixString(string(raw[5:]))))
	return nil
}

// Query returns a Query from a server
func (s *Server) Query(timeout, deadline time.Duration) (*Query, error) {
	start := time.Now()
	conn, err := net.DialTimeout("udp", fmt.Sprintf("%s:%d", s.IP, s.Port), timeout)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	if err := conn.SetDeadline(time.Now().Add(deadline)); err != nil {
		return nil, err
	}
	q := &Query{
		Plugins: make([]Plugin, 0),
		Players: make([]string, 0),
	}
	q.Latency = time.Since(start).Round(time.Millisecond)

	if err := s.handshake(conn); err != nil {
		return nil, err
	}

	if _, err := conn.Write([]byte(fmt.Sprintf("\xFE\xFD\x00\x00\x00\x00\x01%s\x00\x00\x00\x00", s.packChallenge()))); err != nil {
		return nil, err
	}

	raw := make([]byte, bytes.MinRead*5)
	if _, err := conn.Read(raw[:]); err != nil {
		return nil, err
	}

	if len(raw) == 0 {
		return nil, errors.New("no data returned")
	}

	parseData(q, raw)

	return q, nil
}

func parseData(q *Query, raw []byte) {

	// https://wiki.vg/Query#Response_3
	raw = raw[16:]
	data := strings.Split(string(raw[:]), "\x00\x01player_\x00\x00")

	// First chunk of data
	kv := strings.Split(data[0], "\x00")
	for idx, k := range kv {
		if idx%2 != 0 || idx+1 == len(kv) {
			continue
		}
		val := kv[idx+1]
		switch k {
		case "hostname":
			q.MOTD = val
		case "gametype":
			q.GameType = val
		case "game_id":
			q.GameID = val
		case "version":
			q.Version = val
		case "map":
			q.Map = val
		case "numplayers":
			q.CurrentPlayers = mustInt(val)
		case "maxplayers":
			q.MaxPlayers = mustInt(val)
		case "hostport":
			q.HostPort = mustInt(val)
		case "hostip":
			q.HostIP = val
		case "plugins":
			listing := strings.Split(val, ":")
			s := strings.Fields(listing[0])
			name := s[0]
			base := ""
			version := s[2]
			if len(s) == 4 {
				base = s[2]
				version = s[3]
			}
			q.ServerMod = ServerMod{
				Name:    name,
				Base:    base,
				Version: version,
			}
			for _, plugin := range strings.Split(listing[1], ";") {
				nv := strings.Fields(strings.TrimSpace(plugin))
				p := Plugin{
					Name:    nv[0],
					Version: nv[1],
				}
				q.Plugins = append(q.Plugins, p)
			}
		}
	}

	// Second chunk of data
	for _, player := range strings.Split(data[1], "\x00") {
		if strings.Trim(player, "\x00 ") == "" {
			continue
		}
		q.Players = append(q.Players, player)
	}
}
