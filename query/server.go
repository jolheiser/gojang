package query

import (
	"encoding/binary"
	"strconv"
	"strings"
	"time"
)

const (
	numFields       = 6
	DefaultTimeout  = time.Second * 5
	DefaultDeadline = time.Second * 5
)

// Server is a Minecraft server
type Server struct {
	IP   string
	Port int

	challenge uint32
}

func (s *Server) packChallenge() []byte {
	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, s.challenge)
	return bs
}

// NewServer returns a Server
func NewServer(ip string, port int) *Server {
	return &Server{
		IP:   ip,
		Port: port,
	}
}

func fixString(input string) string {
	return strings.ReplaceAll(input, "\000", "")
}

func mustInt(input string) int {
	if out, err := strconv.ParseInt(fixString(input), 10, 64); err == nil {
		return int(out)
	}
	return 0
}
